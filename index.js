/**bài 1: tính tiền lương nhân viên
 * Input:
 * Lương 1 ngày: 100.000
 * số ngày làm: 25 ngày
 * 
 * 
 * Các bước xử lý:
 * tạo biến lưu giá trị lương 1 ngày và số ngày làm;
 * tạo biến lưu kết quả tiền lương;
 * áp dụng công thức: tienLuong = lương 1 ngày * số ngày làm;
 * 
 * 
 * Output:số lương 25 ngày = 2.500.000
 */

var luong1ngay = 100.000;
var soNgayLam = 25;
var tienLuong =null;

tienLuong = 100000 * 25;
console.log("tienLuong", tienLuong);


/**
 * bài 2: Tính giá trị trung bình
 * Ipnput:
 * 
 * num1 = 5
 * num2 = 6
 * num3 = 7
 * num4 = 8 
 * num5 = 9
 * 
 * 
 * các bước xử lý: 
 * tạo biến lưu giá trị của 5 số thực
 * tạo biến tính trung bình của 5 số thực
 * tạo biến lưu kết quả
 * áp dụng công thức: (num1 + num2 +num3 + num4 + num5) / 5
 * 
 * 
 * 
 * 
 * output:trung bình của 5 số này = 7
 */

var num1 = 5;
var num2 = 6;
var num3 = 7;
var num4 = 8;
var num5 = 9;
var mean = null;

mean = (5 + 6 + 7 + 8 + 9) / 5;
console.log("mean", mean);


/**
 * bài 3: Quy Đổi Tiền
 * Input: giá 1 USD : 23.500 VNĐ
 * 
 *
 * Các bước xử lý: 
 * tạo biến lưu giá trị
 * tạo biến lưu kết quả
 * áp dụng công thức: USD * VNĐ
 * 
 * 
 * 
 * output: 5 USD = 117.500 VNĐ
 */

var usd = 5;
var vnd = 23500;
var moneyExchange = null;

moneyExchange = 5 * 23500;
console.log("moneyExchange",moneyExchange);


/**
 * bài 4: Tính  diện tích chu vi hình chữ nhật
 * 
 * Input: chiều dài = 15 ;  chiều rộng = 30 ;
 * 
 * các bước xử lý:
 * áp dụng công thức tính diện tích:  diện tích = chiều dài * chiều rộng
 * áp dụng công thức tính chu vi:  chu vi = (chiều dài + chiều rộng) * 2;
 * 
 * 
 * output: 
 * dien tich=  450
 * chu vi =   90
*/

var chieuDai = 15;
var chieuRong = 30;
var dienTich = null;
dienTich = 15 * 30;
console.log("dienTich",dienTich);

var chieuDai = 15;
var chieuRong = 30;
var chuVi = null;
chuVi = (15 + 30) * 2;
console.log("chuVi",chuVi);

/**
 * bài 5: Tính tổng 2 ký số
 * 
 * input : số nguyên dương n có 2 ký số 
 * 
 * 
 * Các bước xử lý: 
 * - tạo biến n,unit,ten,hundred,sum
 * - gán giá trị cho n;
 * - tách số hàng trăm theo công thức hundred = Math.floor(n/100)
 * - tách số hàng chục theo công thức ten = Math.floor(n%100/10)
 * - tách số hàng đơn vị theo công thức unit = n%10
 * - tạo biến kết quả
 * 
 * 
 * output: tổng 2 ký số = ?
 */


var n = 73, unit, ten, hundred, sum;
hundred = Math.floor(n/100);
ten = Math.floor(n%100/10);
unit = n%10;
sum = ten + unit;
console.log("sum", sum)




 

